package client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.sql.SQLException;
import java.util.Scanner;
import constants.Constants;
import entities.Message;
import exception.InvalidPortException;
import exception.InvalidTopicException;
import exception.InvalidUserChoiceException;
import services.ClientService;
import services.PublisherService;
import services.SubscriberService;

public class ClientApp 
{
	static Scanner input = new Scanner(System.in);
	static Socket socket = null;
	static DataOutputStream publisherOutputStream = null;
	static DataInputStream publisherInputStream = null;
	static DataInputStream subscriberInputStream = null;
	static DataOutputStream subscriberOutputStrem = null;

	public static void main(String args[]) throws InvalidPortException 
	{
		ClientService clientService = null;
		PublisherService publisherService = null;
		SubscriberService subscriberService = null;
		boolean isValidAddress = false;
		int userChoice;
		String userInput = "";

		do
		{
			try 
			{
				System.out.println("Enter 'P' to Publish and 'S' to subscribe and 'C' to close the app");
				userInput = input.next().toLowerCase();;

				if(userInput.equals("p"))
				{
					publisherService = new PublisherService(Constants.host, Constants.publisherPort);
					clientService = publisherService;
					socket = publisherService.CreateSocket();
					getPublisherService(publisherService, true);
				}
				else if(userInput.equals("s"))
				{
					subscriberService = new SubscriberService(Constants.host, Constants.subscriberPort);
					clientService = subscriberService;
					socket = subscriberService.CreateSocket();
					getSubscriberService(subscriberService, true);
				}
				else if(userInput.equals("c")) 
				{
					break;
				}
				else 
				{ 
					throw new InvalidUserChoiceException("Please select a valid choice"); 
				}
					
			} 
			catch (Exception exception) 
			{
				throw new InvalidPortException("Connection is refused because port already in use");
			}
		} while(!(userInput == "p") ||(userInput == "s"));
		System.out.println("Thank you for using the application!!");
	}
	
	public static void getPublisherService(PublisherService publisherService, boolean isValidClientAddress) throws IOException, SQLException
	{
		int userChoice;
		publisherOutputStream = new DataOutputStream(socket.getOutputStream());
		publisherInputStream = new DataInputStream(socket.getInputStream());
		publisherService.setClientData(getClientName(), publisherOutputStream);
		int topicCount = publisherService.getAvailableTopicNames(publisherInputStream);
		
		do 
		{
			System.out.println("Press 1 to enter data and 2 to close the connection");
			input = new Scanner(System.in);

			userChoice = input.nextInt();
			try 
			{
				switch (userChoice) 
				{
					case 1: 
					{
						publisherService.printAvailableTopics();
						int topicId = publisherService.getTopicId();
						
						if(topicId > topicCount )
						{
							throw new InvalidTopicException("Please enter a valid topic name");
						}
						else
						{
							String publisherData = publisherService.getPublisherData();
							Message message = new Message();
							message.setTopicId(topicId);
							message.setMessage(publisherData);
							publisherService.sendpublisherDataToServer(message, publisherOutputStream);
							publisherService.getPublisherResponseFromServer(publisherInputStream);
							continue;
						}
					}
					case 2: 
					{
						Message message = new Message();
						message.setMessage("quit");
						publisherService.sendpublisherDataToServer(message, publisherOutputStream);
						publisherService.closePublisherConnection(publisherOutputStream);
						isValidClientAddress = false;
						continue;
					}
					default: 
					{
						isValidClientAddress = true;
						throw new InvalidUserChoiceException("Please select a valid choice");
					}
				}
			}
			catch(InvalidTopicException ex)
			{
				System.out.println(ex.getMessage());
			}
			catch(InvalidUserChoiceException ex)
			{
				System.out.println(ex.getMessage());
			}
			catch(Exception exception)
			{
				System.out.println("Something went wrong.. Please try again later");
			}
		} while (isValidClientAddress);
	}
	
	public static void getSubscriberService(SubscriberService subscriberService, boolean isValidClientAddress) throws IOException, SQLException
	{
		int userChoice;
		subscriberOutputStrem = new DataOutputStream(socket.getOutputStream());
		subscriberInputStream = new DataInputStream(socket.getInputStream());
		subscriberService.setClientData(getClientName(), subscriberOutputStrem);
		int topicCount = subscriberService.getAvailableTopicNames(subscriberInputStream);

		do
		{
			System.out.println("Press 1 to enter topic and 2 to close the connection");
			input = new Scanner(System.in);
			userChoice = input.nextInt();
			
			try 
			{
				switch (userChoice) 
				{
					case 1: {
						subscriberService.printAvailableTopics();
						int topicId = subscriberService.getTopicId();
						
						if(topicId > topicCount)
						{
							throw new InvalidTopicException("Please enter a valid topic name");
						}
						else
						{
							subscriberService.sendSubscribedTopicId(subscriberOutputStrem, topicId);
							subscriberService.getSubscribedData();
							continue;
						}
					}
	
					case 2: {
						subscriberService.sendSubscribedTopicId(subscriberOutputStrem, -1);
						subscriberService.closeSubscriberConnection(subscriberOutputStrem);
						isValidClientAddress = false;
						break;
					}
					default: 
					{
						isValidClientAddress = true;
						throw new InvalidUserChoiceException("Please select a valid choice");
					}
				}
			} 
			catch(NumberFormatException exe)
			{
				System.out.println("Entered value is not a number");
			}
			catch (InvalidUserChoiceException ex) 
			{
				System.out.println(ex.getMessage());
			}
			
			catch(InvalidTopicException ex)
			{
				System.out.println(ex.getMessage());
			}
			catch(Exception exception)
			{
				System.out.println("Something went wrong.. Please try again later");
			}
		}while(isValidClientAddress);
	}

	private static String getClientName() 
	{
		System.out.println("Enter your name :");
		String clientName = input.next();		
		return clientName;
	}
}
