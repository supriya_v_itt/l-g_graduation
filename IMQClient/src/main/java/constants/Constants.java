package constants;

public class Constants 
{
	public static String host = "127.0.0.1";
	public static int publisherPort = 4444;
	public static int subscriberPort = 1221;
}
