package exception;

public class InvalidUserChoiceException extends Exception
{
	public InvalidUserChoiceException(String errorMessage)
	{
		super(errorMessage);
	}
}
