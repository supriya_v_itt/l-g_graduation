package services;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;
import com.google.gson.Gson;

public class ClientService 
{
	private static String ip;
	private static int port;
	private static Socket socket = null;
	Scanner scan = new Scanner(System.in);
	ArrayList topics;

	public ClientService(String ip, int port) 
	{
		this.ip = ip;
		this.port = port;
	}

	public static Socket createSocketConnection() throws IOException 
	{
		socket = new Socket(ip, port);
		return socket;
	}

	public void setClientData(String clientName, DataOutputStream outputStream) throws IOException
	{
		outputStream.writeUTF(clientName);
	}

	public int getTopicId() 
	{
		System.out.println("Enter the topic number");
		int topicId = scan.nextInt();
		return topicId;
	}

	public int getAvailableTopicNames(DataInputStream inputStream) throws SQLException, IOException
	{
		String availableTopics = inputStream.readUTF();
		topics = new Gson().fromJson(availableTopics, ArrayList.class);
		return topics.size();
	}

	public void printAvailableTopics()
	{
		System.out.println("Available topics are :");
		for(int i = 0; i < topics.size(); i++)
		{
			System.out.println(i+1 + ". " + topics.get(i));
		}
	}
}
