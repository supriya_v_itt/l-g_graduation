package services;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import com.google.gson.Gson;
import entities.Message;


public class PublisherService extends ClientService 
{
	static Socket socket;
	private DataOutputStream output = null;

	public PublisherService(String ip, int port) throws IOException 
	{
		super(ip, port);
	}
	public Socket CreateSocket() throws IOException
	{
		socket = createSocketConnection();
		return socket;
	}

	public String getPublisherData() throws IOException 
	{
		String publisherData = null;
		System.out.println("Enter the data :");
		publisherData = super.scan.next();
		return publisherData;
	}
	
	public String serializePublisherData(Message message)
	{
		Gson gson = new Gson();
		String serializedMessage = gson.toJson(message);
		return serializedMessage;
	}

	public void sendpublisherDataToServer(Message message, DataOutputStream output) throws IOException 
	{
		output.writeUTF(serializePublisherData(message));
	}
	
	public void getPublisherResponseFromServer(DataInputStream inputStream) throws IOException 
	{
		String serverResponse;
		serverResponse = inputStream.readUTF();
		System.out.println("The data published is : " + serverResponse);
	}

	public void closePublisherConnection(DataOutputStream output) throws IOException 
	{
		output.close();
		socket.close();
	}

}