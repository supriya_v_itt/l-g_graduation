package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import constants.Constants;
import entities.ClientData;
import entities.Message;
import protocol.MessageObject;

public class QueueDataObject 
{
	Connection connection = null;
	String localPort;
	
	public QueueDataObject()
	{
		getDatabaseConnection();
	}
	
	public Connection getDatabaseConnection()
	{
		try
		{
			DriverManager.registerDriver(new com.microsoft.sqlserver.jdbc.SQLServerDriver());
			connection = DriverManager.getConnection(Constants.connectionUrl);
			return connection;
		}
		catch(Exception exe)
		{
			exe.printStackTrace();
			return null;
		}
	}
	
	public boolean insertClient(String clientName, String clientType) 
	{
		ClientData clientData = new ClientData();
		clientData.setClientName(clientName);
		clientData.setClientType(clientType);
		try 
		{
			PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO ClientData VALUES (?,?);");
	        preparedStatement.setString(1, clientData.getClientName());
	        preparedStatement.setString(2, clientData.getClientType());
	        preparedStatement.executeUpdate();
	        return true;
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean insertMessage(MessageObject message, int topicId) 
	{
		Message imqMessage = new Message();
		imqMessage.setTopicId(topicId);
		imqMessage.setMessage(message.getData());
		try 
		{
			PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO MessageQueue VALUES (?,?,?);");
	        preparedStatement.setInt(1, imqMessage.getTopicId());
	        preparedStatement.setString(2, imqMessage.getMessage());
	        preparedStatement.setString(3, message.getCreateTime());
	        preparedStatement.executeUpdate();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public int findTopicId(String topicName) throws SQLException 
	{
		PreparedStatement preparedStatement = connection.prepareStatement("SELECT id FROM Topic WHERE topic_name = ?");
        preparedStatement.setString(1, topicName);
        ResultSet resultList = preparedStatement.executeQuery();
        resultList.next();
		return resultList.getInt("id");
	}

	public ResultSet findMessagesByTopicId(int topicId) 
	{
		try 
		{
			PreparedStatement preparedStatement = connection.prepareStatement("Select top 1 message from MessageQueue where topicId = ?;");
	        preparedStatement.setInt(1, topicId);
	        ResultSet result = preparedStatement.executeQuery();
	        return result;
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			return null;
		}
	}
	
	public ResultSet getAllMessages() 
	{
		try 
		{
			PreparedStatement preparedStatement = connection.prepareStatement("Select id,createdTime from MessageQueue;");
			ResultSet result = preparedStatement.executeQuery();
			return result;
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			return null;
		}
	}
	
	public ArrayList getAvailableTopics() throws SQLException 
	{
		ArrayList availableTopics = new ArrayList();
		java.sql.Statement statement = connection.createStatement();
		ResultSet resultList = statement.executeQuery("SELECT topic_name FROM Topic");
		while(resultList.next())
		{
			 availableTopics.add(resultList.getString(1));
		}
		return availableTopics;
	}

	public boolean deleteTimedOutMessage(int id) 
	{
		try 
		{
			PreparedStatement preparedStatement = connection.prepareStatement(
					"Insert into DeadLetterQueue select id,topicId,message,createdTime from MessageQueue where id = ?; Delete from MessageQueue where id = ?;");
	        preparedStatement.setInt(1, id);
	        preparedStatement.setInt(2, id);
	        preparedStatement.executeUpdate();
	        return true;
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			return false;
		}
	}
}
