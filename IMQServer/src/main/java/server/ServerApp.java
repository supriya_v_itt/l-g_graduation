package server;

import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import constants.Constants;
import exception.InvalidPortException;
import services.PublisherHandler;
import services.QueueService;
import services.SubscriberHandler;

public class ServerApp 
{
	public static void main(String args[]) throws InvalidPortException 
	{
		System.out.println("Server Started..");

		Thread deadLetterQueueThread = new Thread()
		{
			public void run()
			{
				try
				{
					while(true)
					{
						deadLetterQueueHandler();
						Thread.sleep(3000000);
					}
				}
				catch(Exception exe) 
				{
					exe.printStackTrace();
				}
			}
		};

		Thread publisherThread = new Thread()
		{
		    public void run()
		    {
		    	try 
		    	{
					handlePublishers();
				} 
		    	catch (InvalidPortException e) 
		    	{
					e.printStackTrace();
				}
		    }
		};
		Thread subscriberThread = new Thread()
		{
			public void run() 
			{
				try 
				{
					handleSubscribers();
				} 
				catch (InvalidPortException e) 
				{
					e.printStackTrace();
				}
			}
		};
		
		publisherThread.start();
		subscriberThread.start();
		deadLetterQueueThread.start();
	}

	private static void deadLetterQueueHandler() throws SQLException, ParseException, InterruptedException 
	{
		System.out.println("Traversing queue for dead messages");;
		QueueService queueService = new QueueService();
		queueService.deadLetterQueueHandler();
	}
	
	private static void handlePublishers() throws InvalidPortException 
	{
		try
		{
			ServerSocket serverPublisherSocket = new ServerSocket(Constants.publisherPort);
			while(true)
			{
				Socket publisherSocket = serverPublisherSocket.accept();
				if(publisherSocket.getLocalPort() == Constants.publisherPort)
				{
					PublisherHandler publisherHandler = new PublisherHandler(publisherSocket);
					
					publisherHandler.start();
				}
			}
		}
		catch (Exception exception) 
		{
			throw new InvalidPortException("Connection is refused because port already in use");
		}
	}

	private static void handleSubscribers() throws InvalidPortException 
	{
		try
		{
			ServerSocket serverSubscriberSocket = new ServerSocket(Constants.subscriberPort);
			while(true)
			{
				Socket subscriberSocket = serverSubscriberSocket.accept();
				if(subscriberSocket.getLocalPort() == Constants.subscriberPort)
				{
					SubscriberHandler subscriberHandler = new SubscriberHandler(subscriberSocket);
					subscriberHandler.start();
				}
			}
		}
		catch (Exception exception) 
		{
			throw new InvalidPortException("Connection is refused because port already in use");
		}
	}
}
