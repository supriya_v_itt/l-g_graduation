package services;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.sql.SQLException;
import java.util.Date;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import com.google.gson.JsonSyntaxException;
import entities.Message;
import protocol.MessageObject;


public class PublisherHandler extends Thread 
{
	private Socket socket = null;
	private DataInputStream input = null;
	private DataOutputStream output = null;
	QueueService queueService = new QueueService();

	public PublisherHandler(Socket publisherSocket) 
	{
		this.socket = publisherSocket;
		try 
		{
			input = new DataInputStream(socket.getInputStream());
			output = new DataOutputStream(socket.getOutputStream());
		} 
		catch (IOException exception) 
		{
			exception.printStackTrace();
		}
	}

	public Message getPublisherData() throws IOException, ClassNotFoundException 
	{
		String publisherData = null;
		Message message = new Message();
		publisherData = input.readUTF();
		try
		{
			JSONParser parser = new JSONParser();
			JSONObject json = (JSONObject) parser.parse(publisherData); 
			message.setMessage((json.get("message")).toString());
			message.setTopicId(Integer.parseInt(json.get("topicId").toString()));

			return message;
		}
		catch(Exception ex)
		{
			System.out.println(ex);
		}
		return message;
	}
	
	public void sendPublisherResponse(String message, DataOutputStream output) throws IOException, JsonSyntaxException, SQLException 
	{
		output.writeUTF((String) message);
	}

	public void savePublishedData(MessageObject message, int topicId) 
	{
		queueService.pushMessageToQueue(message, topicId);
	}

	public void run() 
	{
		MessageObject messageObject = null;
		Message message = new Message();

		try {
			saveClientName(getClientName());
			sendAvailableTopics();

		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		do
		{
			try 
			{
				message = getPublisherData();
				if(message.getMessage().equals("quit"))
				{
					continue;
				}
				else
				{
					messageObject = new MessageObject();
					messageObject.setData(message.getMessage());
					messageObject.setCreateTime(queueService.formatter.format(new Date()));
					savePublishedData(messageObject, message.getTopicId());
					sendPublisherResponse(message.getMessage(), output);
				}
			} 
			catch (IOException exception) 
			{
				exception.printStackTrace();
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		} while	(!(message.getMessage().equals("quit")));
	}

	private void sendAvailableTopics() throws IOException, SQLException 
	{
		output.writeUTF(queueService.getAvailableTopics());		
	}

	private void saveClientName(String clientName) 
	{
		queueService.insertClientName(clientName, "Publisher");
	}

	private String getClientName() throws IOException 
	{
		String clientName = input.readUTF();
		return clientName;
	}
}