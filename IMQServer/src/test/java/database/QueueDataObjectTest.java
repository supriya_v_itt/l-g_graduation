package database;

import java.sql.Connection;
import java.sql.SQLException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import junit.framework.Assert;
import protocol.MessageObject;

class QueueDataObjectTest {

	QueueDataObject queueDataObject;
	@BeforeEach
	void createObject()
	{
		queueDataObject = new QueueDataObject();
		queueDataObject.getDatabaseConnection();
	}

	@Test
	void testGetDatabaseConnection() {
		Connection actual = queueDataObject.getDatabaseConnection();
		Assert.assertNotNull(actual);
	}

	@Test
	void testInsertMessage() {
		MessageObject message = new MessageObject();
		message.setData("test data");
		message.setCreateTime("27/02/2021 02:27:36");
		boolean actual = queueDataObject.insertMessage(message, 100);
		boolean expected = true;
		Assert.assertEquals(expected, actual);
	}

	@Test
	void testFindTopicId() throws SQLException {
		int actual = queueDataObject.findTopicId("java");
		Assert.assertEquals(1, actual);
	}
}
