package services;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.SQLException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import exception.NoMessagesExeption;
import junit.framework.Assert;
import protocol.MessageObject;

class QueueServiceTest {

	QueueService queueService;
	
	@BeforeEach
	void createObject()
	{
		queueService = new QueueService();
	}
	
	@Test
	void testPushMessageToQueue() {
		MessageObject message = new MessageObject();
		message.setData("test data");
		message.setCreateTime("27/02/2021 02:27:36");
		boolean actual = queueService.pushMessageToQueue(message, 100);
		boolean expected = true;
		Assert.assertEquals(expected, actual);
	}

	@Test
	void testGetAvailableTopics() throws SQLException {
		String actual = queueService.getAvailableTopics();
		String expected = "[\"java      \",\"cs        \"]";
		Assert.assertEquals(expected, actual);
	}

	
}
